from kaldi_io import read_mat_scp
from keras.models import load_model
import keras.backend as K
import sys
import numpy as np


#Load the feature file with labels
feat_scp_file=sys.argv[1]
model_file=sys.argv[2]


mfcc=[]
spks=[]
maxlen=250	#5 seconds length frame shift 20ms

for key,mat in read_mat_scp(open(feat_scp_file,'r')):
	if mat.shape[0]<maxlen:
		mat=np.pad(mat,((0,maxlen-mat.shape[0]),(0,0)),'constant',constant_values=0)
	else:
		l=(mat.shape[0]-maxlen)/2
		mat=mat[l:l+maxlen,:]

	mfcc.append(mat)
	spks.append(key)

model=load_model(model_file)
	
mfcc=np.array(mfcc)


first_dense_layer=K.function([model.layers[0].input,K.learning_phase()],[model.layers[-4].output])

out_feats=first_dense_layer([mfcc,0])[0]   #testing phase

np.savez(sys.argv[3],key=spks,feats=out_feats)
