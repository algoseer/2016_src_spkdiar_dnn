from kaldi_io import read_mat_scp
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
#from keras.layers import Masking, TimeDistributed
from keras.layers.recurrent import LSTM

from keras.layers.convolutional import AtrousConvolution1D
from keras.layers.pooling import MaxPooling1D


import sys
import numpy as np


#Load the feature file with labels
feat_scp_file=sys.argv[1]
maxlen=250	#5 seconds length frame shift 20ms
batch_size=50
n_epochs=50
nfilter=5
subsample_rate=3
spks=[]
mfcc=[]

for key,mat in read_mat_scp(open(feat_scp_file,'r')):
	if mat.shape[0]<maxlen:
		mat=np.pad(mat,((0,maxlen-mat.shape[0]),(0,0)),'constant',constant_values=0)
	else:
		l=(mat.shape[0]-maxlen)/2
		mat=mat[l:l+maxlen,:]

	mfcc.append(mat)
	spks.append(key[:3])

	
mfcc=np.array(mfcc)
inp_dim=mfcc.shape[2]
nb_classes=len(set(spks))
_,spks=np.unique(spks,return_inverse=True)
labels=np.eye(nb_classes)[spks]

print "num classes =",nb_classes
print mfcc.shape,spks.shape

#setup network architecture

model = Sequential()

model.add(AtrousConvolution1D(64,nfilter,subsample_length=subsample_rate,input_shape=(maxlen,inp_dim)))
model.add(Activation('relu'))
model.add(AtrousConvolution1D(64,nfilter,subsample_length=subsample_rate))
model.add(Activation('relu'))
model.add(MaxPooling1D(pool_length=subsample_rate))
model.add(Dropout(0.25))


model.add(LSTM(128,return_sequences=True))
model.add(Dropout(0.25))
model.add(LSTM(128))
model.add(Dropout(0.25))
model.add(Dense(256))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nb_classes))
model.add(Activation('softmax'))


model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
model.fit(mfcc,labels, batch_size=batch_size, nb_epoch=n_epochs,validation_split=0.2)
