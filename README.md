##VoiceNET##

Welcome to Voicenet v 1.0.

Voicenet is an attempt to train a network that can learn features relevant for speaker voice discrimination. This can be useful in tasks such as speaker recognition and diarization.


## Dependencies ##
- Kaldi : For feature extraction
- kaldi_io : this library is used for loading data in python https://github.com/vesis84/kaldi-io-for-python
- keras : this is the learning interface most of the code is written in
- theano/tensorflow : keras code is agnostic to whichever engine you use as long it is configured properly

## Network architecture ##


 [ MFCC ] ==>
[ 1D Atrous Conv layers 64 filters ]
[ 1D Atrous Conv layers 64 filters ]
[ MaxPool 1D ] 
[ LSTM  layer 128 cells ] 
[ LSTM  layer 128 cells ] 
[ Dense layer 256 nodes ] 
[softmax layer] ==>
spk. id (1-hot)

Additionally to prevent overfitting, dropout is applied to the LSTM and fully connected layers.


### Training ###
```
python train_voicenet_features.py <kaldi-mfcc-scp> <model-file>
```

### FC layer features ###
```
python train_voicenet_features.py <kaldi-mfcc-scp> <model-file> <out-feature-file>
```
These 256 dimensional features should be useful for speaker clustering.

### Fine tune model ###
```
python finetune_voicenet.py <kaldi-mfcc-scp> <model-file>
```
This can be used to adapt a pre-trained model to your dataset or retrain just the softmax layer for a new classifiers. This training shouldn't require less samples.


### Pretrained models ###

Coming soon...


### TODO ###
- Train on larger datasets
- Multitask learning to improve robustness (gender, age, spk. id)


### RESUTLS ###

coming soon..


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions